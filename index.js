var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const shellExec = require('shell-exec');
var pjson = require('./package.json');
const port = process.env.PORT || 3000;

const noHeartbeatRebootTimeout = 5*60*1000; // 5 mins
const neverHeartbeatRebootTimeout = 15*60*1000; // 15 mins
const heartbeatRequestInterval = 20*1000; // 5s
const statsReportInterval = 60*1000; // 60s
var lastHeartbeatReceived = -1;
var startUpDateTime = Date.now();
var os = require('os');
var ips = [];
var resolution = { width: 0, height: 0, tried_cec: false, tried_legacy: false, success: false }

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/demo.html');
});

var ifaces = os.networkInterfaces();

Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      ips.push(iface.address);
    } else {
      ips.push(iface.address);
    }
    ++alias;
  });
});

console.log(pjson.description + "("+pjson.name+") v"+pjson.version+" starting...");

function enableMonitorCec() {
    shellExec("echo 'on 0' | cec-client -s -d 1").then(function(res) {
        console.log(res);
        setTimeout(() => { shellExec("echo 'as' | cec-client -s -d 1").then(function(res){
            console.log(res);
            detectResolution();
        }).catch(console.log); }, 12500); // wait for 12.5s to give TV time to wakeup
    }).catch(console.log);
}

function enableMonitorLegacy() {
    shellExec("sudo tvservice -p; sudo chvt 1; sudo chvt 7;").then(function(res) {
        console.log(res);
        setTimeout(() => { detectResolution(); }, 12500); // wait for 12.5s to give TV time to wakeup
    }).catch(console.log);
}

io.on('connection', function (socket) {
    socket.on('chat message', function (msg) {
        console.log('emit:' + msg);
        io.emit('chat message', msg);
    });
    socket.on('DISPLAY', function (msg) {
        console.log('DISPLAY: ' + msg);
        if (msg == 'pc-on') {
            enableMonitorLegacy();
        }

        if (msg == 'on') {
            enableMonitorCec();
        }


        if (msg == 'pc-off') {
            shellExec("tvservice -o").then(console.log).catch(console.log);
        }
        
        if (msg == 'off') {
            shellExec("echo 'standby 0' | cec-client -s -d 1").then(console.log).catch(console.log);
        }
    });
    socket.on('REBOOT', function (msg) {
        console.log('REBOOT: ' + msg);
        shellExec('reboot').then(console.log).catch(console.log);
    });
    socket.on('SHUTDOWN', function (msg) {
        console.log('SHUTDOWN ' + msg);
        shellExec('shutdown -h 0').then(console.log).catch(console.log);
    });
    socket.on('SELFUPDATE', function (msg) {
        console.log('SELFUPDATE [cd ' + __dirname + ' && git fetch --all && git reset --hard origin/master  && chmod +x selfupdate.sh]' + msg);
        shellExec('cd ' + __dirname + ' && git fetch --all && git reset --hard origin/master && chmod +x selfupdate.sh').then(console.log).catch(console.log);
    });
    socket.on('RESETIDENT', function (msg) {
        console.log('RESETIDENT ' + msg);
        io.emit('RESETIDENT', msg);
    });
    socket.on('HEARTBEAT', function (msg) {
        if (msg == 'alive') {
            lastHeartbeatReceived = Date.now();
        }
        console.log('received HEARTBEAT' + msg);
    });
    socket.on('VERSION', function (msg) {
        console.log("Software Version: " + pjson.description + "("+pjson.name+") v"+pjson.version);
        io.emit('VERSION', pjson.version);
    });

    socket.on('RESOLUTION', function (msg) {
        detectResolution();
    });

    socket.on('CLEANUP', function (msg) {
        console.log(pjson.description + "("+pjson.name+") v"+pjson.version+" cleanup-task (once)");
        shellExec("apt-get purge -y wolfram-engine && sudo apt-get purge -y libreoffice* && sudo apt-get autoremove -y && sudo apt-get clean").then(console.log).catch(console.log);
    });

    socket.on('PRINT', function (msg) {
        console.log('PRINT: ' + msg);
        

        // TODO: Implement downloading of pdf and trigger lp call - msq will ne som json like
        // { token: "", alert: 0, copies: 0 } and may contain a printer name (?)
    });
});

setInterval(() => {
    io.emit('HEARTBEAT', 'request');
    console.log('emitting HEARTBEAT request');
    // check if reboot needed
    if (lastHeartbeatReceived == -1 && Date.now() - startUpDateTime > neverHeartbeatRebootTimeout) {
        console.log('### REBOOT - NEVER RECEIVED HEARTBEAT !!! ###');
        shellExec('reboot').then(console.log).catch(console.log);
    }
    if (lastHeartbeatReceived > -1 && Date.now() - lastHeartbeatReceived > noHeartbeatRebootTimeout) {
        console.log('### REBOOT - NO HEARTBEAT AFTER 5 MINS !!! ###');
        shellExec('reboot').then(console.log).catch(console.log);
    }
}, heartbeatRequestInterval);


function emitStats() {
    console.log('emitting Stats');
    io.emit('IPS', ips);
    io.emit('VERSION', pjson.version);
    detectResolution();
}

function detectResolution() {
    if (resolution.success) {
        io.emit('RESOLUTION', resolution);
    } else {
        shellExec('tvservice -s').then(function(res){
            console.log("we got response from tvservice: " + res.stdout);
            if (res.stdout.indexOf('TV is off') !== -1) {
                // TV is off!
                if (!resolution.tried_cec) {
                    resolution.tried_cec = true;
                    enableMonitorCec();
                } else {
                    if (!resolution.tried_legacy) {
                        resolution.tried_legacy = true;
                        enableMonitorLegacy();
                    }
                }
            }
            var displayParts = explode(',', res.stdout);
            if (displayParts.length>=1) {
                var resolutionParts = explode('@', displayParts[1]);
                if (resolutionParts.length>0) {
                    var whParts = explode('x', resolutionParts[0]);
                    resolution.width = parseInt(whParts[0].trim());
                    resolution.height = parseInt(whParts[1].trim());
                    resolution.success = true;
                    io.emit('RESOLUTION', resolution);
                    console.log('Res: ' + resolution.width + " x " + resolution.height );
                }
            }
        }).catch(console.log);
    }
    
}

function explode(delimiter, str) {
    if (str.includes(delimiter)) {
        return str.split(delimiter);
    }
    return [];
}


http.listen(port, function () {
    console.log('listening on *:' + port);
});

emitStats();
setInterval(() => {
    emitStats();
}, statsReportInterval);


