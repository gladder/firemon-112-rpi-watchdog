# Firemon 112 RPI Watchdog
This Software is needed to monitor Firemon WebKiosk and reboot if there is any trouble with website, internet connection, message queue connection etc.
## You will need several packages to be installed
### Plymouth

* apt-get install plymouth plymouth-themes pix-plym-splash
* copy splash.png
* * to /usr/share/plymouth/themes/pix
* * to /home/pi (set as desktop background by hand after loggin into desktop session)
* sudo joe /boot/config.txt -> append disable_splash=1

#### joe /usr/share/plymouth/themes/pix/pix.script
    screen_width = Window.GetWidth();
    screen_height = Window.GetHeight();
    
    theme_image = Image("splash.png");
    image_width = theme_image.GetWidth();
    image_height = theme_image.GetHeight();
    
    scale_x = image_width / screen_width;
    scale_y = image_height / screen_height;
    
    flag = 1;
    
    if (scale_x > 1 || scale_y > 1)
    {
        if (scale_x > scale_y)
        {
                resized_image = theme_image.Scale (screen_width, image_height / scale_x);
                image_x = 0;
                image_y = (screen_height - ((image_height  * screen_width) / image_width)) / 2;
        }
        else
        {
                resized_image = theme_image.Scale (image_width / scale_y, screen_height);
                image_x = (screen_width - ((image_width  * screen_height) / image_height)) / 2;
                image_y = 0;
        }
    }
    else
    {
            resized_image = theme_image.Scale (image_width, image_height);
            image_x = (screen_width - image_width) / 2;
            image_y = (screen_height - image_height) / 2;
    }

    if (Plymouth.GetMode() != "shutdown")
    {
            sprite = Sprite (resized_image);
            sprite.SetPosition (image_x, image_y, -100);
    }
    
    
    fun message_callback (text) {
            sprite.SetImage (resized_image);
    }
    
    Plymouth.SetUpdateStatusFunction(message_callback);


### OpenBox
apt-get install  --no-install-recommends xserver-xorg x11-xserver-utils xinit openbox

### Chromium
apt-get install chromium-browser

### LightDM
apt-get install lightdm

### unclutter
apt-get install unclutter

### PM2 to handle startup of this WatchDog 
apt-get install pm2

### Cups will be needed to print stuff - be sure to correctly setup printer (as system default!)
apt-get install cups

## do raspi-config
* network options->hostname
* boot options->desktop cli->desktop autologin
* localization->timezone
* advanced->overscan no
* advanced->memorysplit

## edit /boot/config.txt

    disable_overscan=1
    overscan_left=24
    overscan_right=24
    overscan_top=16
    overscan_bottom=16

## edit /boot/cmdline.txt
* dwc_otg.lpm_enable=0 console=serial0,115200 console=tty3 root=/dev/mmcblk0p7 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait splash quiet plymouth.ignore-serial-consoles logo.nologo vt.global_cursor_default=0


## How to Setup Kiosk to start Monitor on startup
You will need to edit /etc/xdg/lxsession/LXDE-pi/autostart
File should contain:

    @lxpanel --profile LXDE-pi
    @pcmanfm --desktop --profile LXDE-pi
    @xscreensaver -no-splash
    point-rpi
    
    # Disable any form of screen saver / screen blanking / power management / cursor
    @unclutter
    @xset s off
    @xset -dpms
    @xset s noblank
    
    # Allow quitting the X server with CTRL-ATL-Backspace
    setxkbmap -option terminate:ctrl_alt_bksp
    
    # Start Chromium in kiosk mode
    sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/'Local State'
    sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":"[^"]\+"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
    @sleep 10
    @chromium-browser --disable-infobars --check-for-update-interval=31536000 --kiosk --kiosk-printing --remote-debugging-port=9222 --autoplay-policy=no-user-gesture-required --disable-plugins https://ffw.04641.de/monitor

You can forward remote debugging port by SSH: ssh -L 80:localhost:9222 pi@{IP} (which will make remote console available through localhost:80 using chrome / chromium)

## pm2 ecosystem.config.js

    module.exports = {
      apps : [{
        name: 'rpi-watchdog',
        script: 'index.js',
        cwd: '/root/firemon-112-rpi-watchdog/',
        watch: true
      }],
    
      deploy : {
        production : {
          user : 'SSH_USERNAME',
          host : 'SSH_HOSTMACHINE',
          ref  : 'origin/master',
          repo : 'GIT_REPOSITORY',
          path : 'DESTINATION_PATH',
          'pre-deploy-local': '',
          'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
          'pre-setup': ''
        }
      }
    };

### don't forget to setup and make persistent after reboot!
*pm2 delete all
*pm2 start ecosystem.config.js
*pm2 save
*pm2 startup

## SSH Key needed for rpi-watchdog update through Bitbucket (put to /root/.ssh/)
### id_rsa.pub

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCUNlfpYTZ6kEbirYenH685bjuataYNN3JvEcVNRylaydbj8V4klfl5ay7YRsdAxsdFQk1mocGSAGu53mqheUsFJU0Mnkt4m2/Nd0kBbBoNXCH0IZ3ImuJXB41wFxf/8zep78tWuasyS9XAfMlNI17ojGKaydixGLxc1hPzZ8dxZga1DHtTXfNjgr6lJ/fD6mLkXDAw4lwJip7EOl8HrDcovR88015amasbQLb0TDP+4v3baa0+BYlAgRXDI3rqQHQyCsWEA2tQBvXFjAdJcWTgcLNiamcrzqoFDxXWH3rH2jyvstheWN/X3erwHzikiCd7PbMWsTwscJ7BFOrqEFvv root@FIREMON-112

### id_rsa

    -----BEGIN OPENSSH PRIVATE KEY-----
    b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
    NhAAAAAwEAAQAAAQEAlDZX6WE2epBG4q2Hpx+vOW47mrWmDTdybxHFTUcpWsnW4/FeJJX5
    eWsu2EbHQMbHRUJNZqHBkgBrud5qoXlLBSVNDJ5LeJtvzXdJAWwaDVwh9CGdyJriVweNcB
    cX//M3qe/LVrmrMkvVwHzJTSNe6IximsnYsRi8XNYT82fHcWYGtQx7U13zY4K+pSf3w+pi
    5FwwMOJcCYqexDpfB6w3KL0fPNNeWpmrG0C29Ewz/uL922mtPgWJQIEVwyN66kB0MgrFhA
    NrUAb1xYwHSXFk4HCzYmpnK86qBQ8V1h96x9o8r7LYXljf193q8B84pIgnez2zFrE8LHCe
    wRTq6hBb7wAAA8g6O+0jOjvtIwAAAAdzc2gtcnNhAAABAQCUNlfpYTZ6kEbirYenH685bj
    uataYNN3JvEcVNRylaydbj8V4klfl5ay7YRsdAxsdFQk1mocGSAGu53mqheUsFJU0Mnkt4
    m2/Nd0kBbBoNXCH0IZ3ImuJXB41wFxf/8zep78tWuasyS9XAfMlNI17ojGKaydixGLxc1h
    PzZ8dxZga1DHtTXfNjgr6lJ/fD6mLkXDAw4lwJip7EOl8HrDcovR88015amasbQLb0TDP+
    4v3baa0+BYlAgRXDI3rqQHQyCsWEA2tQBvXFjAdJcWTgcLNiamcrzqoFDxXWH3rH2jyvst
    heWN/X3erwHzikiCd7PbMWsTwscJ7BFOrqEFvvAAAAAwEAAQAAAQBfEbS2WMsPNzig8lqP
    Uv2cwx6skFQhKi3XHPkIJttaBxo2QH74zNKnlZ8Iz+3DmT39KRho+t0lf+uZ4BdZ2GnMtS
    6FJ4yVom7GxvsnVGYpp62H4YBKSz9gP9Hz0nxsi2BrOwudZ59utcI4pFKjjkq0tEgTreG3
    R3ybuB44BZujGqrOKOLA0S2vZVKEMPZJuB5BHlxzYq/hEV+1LO2Cc18K3NyUfdjS2jQTEs
    RGsSbXvbeAQCsF5/o4ZN+VEC+Gc2SRqHz5T+KxXvGj/YLgaRPvHCMvAyZJqA84vyor4/d0
    ITgRM897Lte+JGmKzq7TvD5cP6kljgTvtYGMNsk+PNPhAAAAgBsMLAy3/l+O76YgXA3Bw3
    KbEO/3gKTztIvAIqmFPAioAiieVKuyofWeJ6WHHPquKyyfqDWUbXMIUuePrjoXZIMSXV5U
    MB7EuSno+8CunFEdsx20YA8J4rQU2E4pajQIDZo12sgsXyuaXDTp5bu7NVbmKUnhwhzuIr
    +X6jjw27U2AAAAgQDE9vSZ5y4AXKhC8c0o+PPdm/Mn2ARyPO+YmZ1b0KpxEsgDgc0LlPjW
    dqg/NV1sga6uim+kK6N7nXGx5NjrWWIFyp2VZ8mObndsVCExIZcJCy1YEH9UN+Qny6o6BO
    I8sG0+b/aEq5f2ZM4cJrm2BR0beI6hmgZR5BJEAHTvaDZM+QAAAIEAwKKhZ8nGUFefGMH2
    CDPf1JmhnvjWSx1V5qSGnEDnSVrCgY4E3a/FpLu+XKyYDVtwgVylie4VB5TdSLJDkPNSbz
    OSdyRebO6bavn9oViVT7BiHsf1o0x+wG6UTeI6jgyOvcGU4r1ruSCnk/C4/Y9C/PQkNskG
    lkIeKT6hrb9SMicAAAAQcm9vdEBGSVJFTU9OLTExMgECAw==
    -----END OPENSSH PRIVATE KEY-----

## Self Update
To be able to run self update take care of registering ssh identity to ssh agent while beeing root: 
* start ssh agent using: eval \`ssh-agent\`
* add identity: ssh-add ~/.ssh/id_rsa

Afterwards you need to remove remote branch origin if this was used by http(s) in the first place.

    git remote rm origin

Afterwards add origin again using ssh

    git remote add origin git@bitbucket.org:gladder/firemon-112-rpi-watchdog.git

Update Script shoult work now - when any change was detected (pm2 has watch enabled) rpi-watchdog should restart automatically using new source-code. Feel free to add this to cron but dont't forget to make it executable (chmod +x) !!!

    # m h  dom mon dow   command
    5 * * * * /root/firemon-112-rpi-watchdog/selfupdate.sh